const LoginPage = require('../pageobjects/login.page')
const HomePage = require('../pageobjects/home.page')
const SecurePage = require('../pageobjects/secure.page')
const userData = require('../../data/user.data')
const CartPage = require('../pageobjects/cart.page')
const CheckoutPage = require('../pageobjects/checkout.page')

describe('My Login application', () => {
  
    //Case 01
    it('should login with valid credentials', async () => {
        browser.url('/');
        browser.maximizeWindow()
        await LoginPage.login(userData.correctCredentials.username, userData.correctCredentials.password)
        await LoginPage.logout()
    })

    //Case 02
    it('should verify with locked out user', async () => {
        browser.url('/');
        await LoginPage.login(userData.lockedOutUser.username, userData.lockedOutUser.password)
        await LoginPage.checkErrorMessage('Epic sadface: Sorry, this user has been locked out.')
    })

    //Case 03
    it('should verify with invalid password', async () => {
        browser.url('/');
        await LoginPage.login(userData.incorrectPassword.username, userData.incorrectPassword.password)
        await LoginPage.checkErrorMessage('Epic sadface: Username and password do not match any user in this service')
    })
    
    //Case 04
    it('should verify with no password', async () => {
        browser.url('/');
        await LoginPage.login(userData.noPassword.username, userData.noPassword.password)
        await LoginPage.checkErrorMessage('Epic sadface: Password is required')
    })

    //Case 05
    it('should verify with invalid email', async () => {
        browser.url('/');
        await LoginPage.login(userData.invalidEMail.username, userData.invalidEMail.password)
    })
})
