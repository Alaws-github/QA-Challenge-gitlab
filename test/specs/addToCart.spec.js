const LoginPage = require('../pageobjects/login.page')
const HomePage = require('../pageobjects/home.page')
const SecurePage = require('../pageobjects/secure.page')
const userData = require('../../data/user.data')
const CartPage = require('../pageobjects/cart.page')
const CheckoutPage = require('../pageobjects/checkout.page')


describe('My Cart Page', () => {

    before('Should log in with valid credentials', async () => {
    browser.url('/');
    browser.maximizeWindow()
    await LoginPage.login(userData.correctCredentials.username, userData.correctCredentials.password)

});

    //Case 01
    it('should verify cart page exist ', async () => {
        await CartPage.btnExist()
    })

        //Case 02
        it('should add a item to cart and remove it', async () => {
            await HomePage.addToCartItem(1)
            await HomePage.navigateToCart()
            await CartPage.itemExist('Sauce Labs Backpack')
            await HomePage.removeItemFromCart('Remove')
        })
        
        //Case 03
        it('should add a item to cart and complete the checkout process', async () => {
           browser.url('/inventory.html');
           await HomePage.addToCartItem(1)
           await HomePage.navigateToCart()
           await CartPage.clickOnCheckoutBtn()
           await CheckoutPage.continueCheckout(userData.checkoutDetails.firstname, userData.checkoutDetails.lastname, userData.checkoutDetails.zipcode)
           await CheckoutPage.clickOnFinishBtn()
           await CheckoutPage.thankyouScreen('Thank you for your order!')
           await browser.pause(3000)
        })

        it('should add more than one item to cart and complete the checkout process', async () => {
            browser.url('/inventory.html');
            await HomePage.addToCartMoreThanOneItem(3)
            await HomePage.navigateToCart()
            await CartPage.clickOnCheckoutBtn()
            await CheckoutPage.continueCheckout(userData.checkoutDetails.firstname, userData.checkoutDetails.lastname, userData.checkoutDetails.zipcode)
            await CheckoutPage.clickOnFinishBtn()
            await CheckoutPage.thankyouScreen('Thank you for your order!')
            await browser.pause(3000)
            })

})