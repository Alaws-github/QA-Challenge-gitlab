//import Page from './page';

class LoginPage {

    get inputUsername () { return $('#user-name') }
    get inputPassword () { return $('#password') }
    get btnSubmit () { return $('#login-button') }
    get menuButton () {return $('#react-burger-menu-btn')}
    get logoutSliderLink () {return $('#logout_sidebar_link')}
    get uiErrorMessage () {return $('h3[data-test=error]')}

    async login (username, password) {
        await this.inputUsername.setValue(username);
        await this.inputPassword.setValue(password);
        await this.btnSubmit.click();
    }

    async checkErrorMessage(message) {
        //await expect(this.uiErrorMessage).toBeDisplayed();
        await expect(this.uiErrorMessage).toHaveTextContaining(message)
    }

    async checkPasswordRequiredMessage(message) {
        await expect(this.passwordRequiredMessage).toBeDisplayed();
        await expect(this.passwordRequiredMessage).toHaveTextContaining(message)
    }

    async logout(){
        await this.menuButton.click()
        await this.logoutSliderLink.click()
    }

    open () {
        return super.open('login');
    }
}

 module.exports = new LoginPage();
