# Real Decoy QA Programming Challenge
Automation Suite for QA Programming Challenge

## Getting Started
Clone this repository:
```sh
$ git clone https://github.com/Alaws-github/QA-Challenge.git
$ cd QA-Challenge
$ npm install
```
### `npm install`

Installs the dependencies and creates the node modules folder.<br />

## Folder Structure
- **data:** This contains data files with test data to be used by test files. *E.g. [users, test case data and any other data that will be used in a test]*
- **page:** This contains page object files which include element selectors and functions that are reused in test files.
- **test/specs:** This contains test files which include the actual tests.

## Executing Tests

npm run test - runs all tests/specs <br />

npm run test -- --spec ./test/specs/addToCart.spec.js - runs a specified test. In this case, the Add To Cart Test<br />

npm run test -- --spec ./test/specs/authentication.spec.js - runs tests in the specified suite. In this case, the Authentication Test<br />

npm run test -- --spec ./test/specs/inventoryCheck.spec.js - runs tests in the specified suite. In this case, the Inventory Check Test<br />

## Generating Test Report

From the root of the project, run the following command: `allure generate --clean && allure open` <br />

This generates and opens the allure report locally, displaying all test results that are currently in the `allure-results` directory. <br />

Results from the report can also be downloaded as a csv file while in the allure interface for sharing.

## Adding a New Suite

*Update the `suites` property in the `wdio.conf.js` file to include the tests/specs that should be included in your new suite.<br/>
    *Eg: `testRun: ['./test/specs/testRun/*.js'] ` would be added for a suite with the name testRun.<br/><br/>
*Update the `scripts` property in the `package.json` file to include the new suite.<br />
    *Eg: `"testRun": "./node_modules/.bin/wdio wdio.conf.js --suite testRun"` would be added for a suite with the name testRun.<br/>

